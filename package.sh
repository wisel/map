#!/bin/bash

source lib/functions.sh
BINARY_DIR=dist/binaries

rm -rf $BINARY_DIR
mkdir -p $BINARY_DIR

function package_spatialite {
  find dist/spatialite/ \
  -name "*.sqlite" \
  | zip -9 -@ $BINARY_DIR/spatialite.zip
}

function package_all_json {
  QUALITY=$1
  title "Packaging ALL with quality $QUALITY"
  find dist/topojson \
  -name "*.$QUALITY.json" \
  | zip -9 -@ $BINARY_DIR/all_${QUALITY}_topojson.zip
}

function package_fra_json {
  QUALITY=$1
  title "Packaging FRANCE with quality $QUALITY"
  find dist/topojson/FR \
  -name "*.$QUALITY.json" \
  | zip -9 -@ $BINARY_DIR/france_${QUALITY}_topojson.zip
}

function package_continent_json {
  QUALITY=$1
  title "Packaging CONTINENTS with quality $QUALITY"

  find dist/topojson \
  -name "world.$QUALITY.json" -or \
  -name "europe.$QUALITY.json" -or \
  -name "european_union.$QUALITY.json" -or \
  -name "asia.$QUALITY.json" -or \
  -name "north_america.$QUALITY.json" -or \
  -name "south_america.$QUALITY.json" -or \
  -name "africa.$QUALITY.json" \
  | zip -9 -@ $BINARY_DIR/continents_${QUALITY}_topojson.zip
}

package_spatialite

package_all_json "low"
package_all_json "medium"
package_all_json "high"

package_continent_json "low"
package_continent_json "medium"
package_continent_json "high"

package_fra_json "low"
package_fra_json "medium"
package_fra_json "high"
