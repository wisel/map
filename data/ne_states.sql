ALTER TABLE ne_states
  ADD id VARCHAR,
  ADD l0_iso VARCHAR,
  ADD l1_iso VARCHAR,
  ADD l2_iso VARCHAR;

update ne_states set region_cod = NULL where region_cod = iso_a2 || '-';
update ne_states set region_cod = NULL where region_cod = iso_a2 || '.';
update ne_states set region_cod = NULL where region_cod = '-99';

update ne_states set gns_region = NULL where gns_region = iso_a2 || '-';
update ne_states set gns_region = NULL where gns_region = iso_a2 || '.';
update ne_states set gns_region = NULL where gns_region = '-99';

update ne_states set gn_a1_code = NULL where gn_a1_code = iso_a2 || '-';
update ne_states set gn_a1_code = NULL where gn_a1_code = iso_a2 || '.';
update ne_states set gn_a1_code = NULL where gn_a1_code = '-99';

update ne_states set code_hasc = NULL where code_hasc = iso_a2 || '-';
update ne_states set code_hasc = NULL where code_hasc = iso_a2 || '.';
update ne_states set code_hasc = NULL where code_hasc = '-99';

update ne_states set iso_3166_2 = NULL where iso_3166_2 = iso_a2 || '-';
update ne_states set iso_3166_2 = NULL where iso_3166_2 = iso_a2 || '.';
update ne_states set iso_3166_2 = NULL where iso_3166_2 = '-99';

update ne_states
SET
  l0_iso = iso_a2,
  l1_iso = COALESCE(region_cod, gns_region, iso_3166_2, gn_a1_code, code_hasc),
  l2_iso = COALESCE(iso_3166_2, gn_a1_code, code_hasc);

update ne_states
  SET id = COALESCE(l2_iso, l1_iso);

update ne_states
  SET l2_iso = NULL WHERE l1_iso = l2_iso;

-- Remove l2_iso for single container
update ne_states ne
  SET l2_iso = NULL
FROM
  ( select l0_iso, l1_iso, count(*)
  from ne_states group by l0_iso, l1_iso
  having count(*) = 1) s
WHERE
ne.l1_iso = s.l1_iso AND
ne.l0_iso = s.l0_iso;

-- get rid of -1 iso_a2
update ne_states ne
  set l0_iso = NULL, l1_iso = NULL, l2_iso = NULL where l0_iso = '-1';
