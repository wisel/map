ALTER TABLE osm_fr_departements
  ADD id VARCHAR,
  ADD name VARCHAR,
  ADD l0_iso VARCHAR,
  ADD l1_insee VARCHAR,
  ADD l1_nuts VARCHAR,
  ADD l1_iso VARCHAR,
  ADD l2_insee VARCHAR,
  ADD l2_nuts VARCHAR,
  ADD l2_iso VARCHAR;

update osm_fr_departements c
SET
  id = d.iso_3166_2,
  name = c.nom,
  l0_iso = 'FR',
  l2_insee = d.id,
  l2_nuts = d.nuts,
  l2_iso = d.iso_3166_2,
  l1_insee = d.region_id
  FROM fra_departements d WHERE
  d.id = c.numero
  ;

-- Update region level
update osm_fr_departements c
SET
  l1_nuts = r.nuts,
  l1_iso = r.iso_3166_2
FROM fra_regions r
WHERE r.id = c.l1_insee;
