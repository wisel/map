
ALTER TABLE osm_fr_regions
  ADD id VARCHAR,
  ADD name VARCHAR,
  ADD l0_iso VARCHAR,
  ADD l1_insee VARCHAR,
  ADD l1_nuts VARCHAR,
  ADD l1_iso VARCHAR;

update osm_fr_regions c SET
  id = r.iso_3166_2,
  name = c.nom,
  l0_iso = 'FR',
  l1_insee = r.id,
  l1_nuts = r.nuts,
  l1_iso = r.iso_3166_2
  FROM fra_regions r WHERE
  r.id = c.numero
  ;
