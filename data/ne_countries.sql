ALTER TABLE ne_countries
  ADD id VARCHAR,
  ADD l0_iso VARCHAR;

-- get rid of -1 iso_a2
update ne_countries ne
  set iso_a2 = NULL where iso_a2 = '-1' or iso_a2 = '-99';

update ne_countries
  set iso_a2 = 'FR' where name = 'France';

update ne_countries
  set iso_a2 = 'NO' where name = 'Norway';

update ne_countries
  set iso_a2 = 'KO' where name = 'Kosovo';

update ne_countries
SET
  id = iso_a2,
  l0_iso = iso_a2;

-- get rid of -1 iso_a2
update ne_countries ne
  set l0_iso = NULL where l0_iso = '-1';
