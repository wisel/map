ALTER TABLE osm_fr_communes
  ADD name VARCHAR,
  ADD id VARCHAR,
  ADD l0_iso VARCHAR,
  ADD l1_insee VARCHAR,
  ADD l1_nuts VARCHAR,
  ADD l1_iso VARCHAR,
  ADD l2_insee VARCHAR,
  ADD l2_nuts VARCHAR,
  ADD l2_iso VARCHAR,
  ADD l3_insee VARCHAR,
  ADD l3_nuts VARCHAR,
  ADD l3_iso VARCHAR;

UPDATE osm_fr_communes c
SET
  id = c.insee,
  name = c.nom,
  l0_iso = 'FR',
  l3_insee = c.insee,
  l3_iso = 'FR-' || c.insee;

-- The mass
UPDATE osm_fr_communes c
SET
  l1_insee = d.region_id,
  l2_insee = d.id,
  l2_nuts = d.nuts,
  l2_iso = d.iso_3166_2
FROM fra_departements d
WHERE
  (d.id = substring(c.insee from 1 for 2) and char_length(d.id) = 2)
  or
  (d.id = substring(c.insee from 1 for 3) and char_length(d.id) = 3)
  or
  d.id = c.insee;

-- Special case RHONE (69)
UPDATE osm_fr_communes c
SET
  l1_insee = (select region_id from fra_departements where id = '69M'),
  l2_insee = '69M',
  l2_nuts = (select nuts from fra_departements where id = '69M'),
  l2_iso = (select iso_3166_2 from fra_departements where id = '69M')
FROM fra_departements d
WHERE
  substring(c.insee from 1 for 2) = '69' and
  c.insee in (select insee from fra_grand_lyon);

UPDATE osm_fr_communes c
SET
  l1_insee = (select region_id from fra_departements where id = '69D'),
  l2_insee = '69D',
  l2_nuts = (select nuts from fra_departements where id = '69D'),
  l2_iso = (select iso_3166_2 from fra_departements where id = '69D')
FROM fra_departements d
WHERE
  substring(c.insee from 1 for 2) = '69' and
  c.insee not in (select insee from fra_grand_lyon);

-- Update region level
update osm_fr_communes c
SET
  l1_nuts = r.nuts,
  l1_iso = r.iso_3166_2
FROM fra_regions r
WHERE r.id = c.l1_insee;
