docker run --name some-postgis -e POSTGRES_PASSWORD=postgis -e POSTGRES_USER=postgis -e POSTGRES_DB=postgis -d mdillon/postgis

Requires:

* mapshaper
* postgis
* ogr2ogr


adm1_code
adm1_cod_1
iso_3166_2
iso_a2           


                                            Table "public.ne_states"
   Column   |          Type          |                        Modifiers         
               | Storage  | Stats target | Description
------------+------------------------+------------------------------------------
---------------+----------+--------------+-------------
 gid        | integer                | not null default nextval('ne_states_gid_s
eq'::regclass) | plain    |              |
 adm1_code  | character varying(10)  |                                          
               | extended |              |
 objectid_1 | integer                |                                          
               | plain    |              |
 diss_me    | integer                |                                          
               | plain    |              |
 adm1_cod_1 | character varying(10)  |                                          
               | extended |              |
 iso_3166_2 | character varying(10)  |                                          
               | extended |              |
 wikipedia  | character varying(254) |                                          
               | extended |              |
 iso_a2     | character varying(2)   |                                          
               | extended |              |
 adm0_sr    | smallint               |                                          
               | plain    |              |
 name       | character varying(100) |                                          
               | extended |              |
 name_alt   | character varying(200) |                                          
               | extended |              |
 name_local | character varying(200) |                                          
               | extended |              |
 type       | character varying(100) |                                          
               | extended |              |
 type_en    | character varying(100) |                                          
               | extended |              |
 code_local | character varying(50)  |                                          
               | extended |              |
 code_hasc  | character varying(10)  |                                          
               | extended |              |
 note       | character varying(254) |                                          
               | extended |              |
 hasc_maybe | character varying(50)  |                                          
               | extended |              |
 region     | character varying(100) |                                          
               | extended |              |
 region_cod | character varying(50)  |                                          
               | extended |              |
 provnum_ne | integer                |                                          
               | plain    |              |
 gadm_level | smallint               |                                          
               | plain    |              |
 check_me   | smallint               |                                          
               | plain    |              |
 scalerank  | smallint               
 datarank   | smallint               |                                                         | plain    |              |
 abbrev     | character varying(10)  |                                                         | extended |              |
 postal     | character varying(10)  |                                                         | extended |              |
 area_sqkm  | double precision       |                                                         | plain    |              |
 sameascity | smallint               |                                                         | plain    |              |
 labelrank  | smallint               |                                                         | plain    |              |
 featurecla | character varying(50)  |                                                         | extended |              |
 name_len   | integer                |                                                         | plain    |              |
 mapcolor9  | smallint               |                                                         | plain    |              |
 mapcolor13 | smallint               |                                                         | plain    |              |
 fips       | character varying(5)   |                                                         | extended |              |
 fips_alt   | character varying(50)  |                                                         | extended |              |
 woe_id     | integer                |                                                         | plain    |              |
 woe_label  | character varying(250) |                                                         | extended |              |
 woe_name   | character varying(100) |                                                         | extended |              |
 latitude   | double precision       |                                                         | plain    |              |
 longitude  | double precision       |                                                         | plain    |              |
 sov_a3     | character varying(3)   |                                                         | extended |              |
 adm0_a3    | character varying(3)   |                                                         | extended |              |
 adm0_label | smallint               |                                                         | plain    |            

 admin      | character varying(200) |                                                         | extended |              |
 geonunit   | character varying(200) |                                                         | extended |              |
 gu_a3      | character varying(3)   |                                                         | extended |              |
 gn_id      | integer                |                                                         | plain    |              |
 gn_name    | character varying(200) |                                                         | extended |              |
 gns_id     | integer                |                                                         | plain    |              |
 gns_name   | character varying(200) |                                                         | extended |              |
 gn_level   | smallint               |                                                         | plain    |              |
 gn_region  | character varying(50)  |                                                         | extended |              |
 gn_a1_code | character varying(10)  |                                                         | extended |              |
 region_sub | character varying(250) |                                                         | extended |              |
 sub_code   | character varying(10)  |                                                         | extended |              |
 gns_level  | smallint               |                                                         | plain    |              |
 gns_lang   | character varying(10)  |                                                         | extended |              |
 gns_adm1   | character varying(10)  |                                                         | extended |              |
 gns_region | character varying(10)  |                                                         | extended |              |
 geom       | geometry(MultiPolygon) |                                                         | main     |              |
 id         | character varying      |                                                         | extended |              |
 l0_iso     | character varying      |                                                         | extended |              |
 l1_iso     | character varying      |                                                         | extended |              |
 admin      | character varying(200) |                                                         | extended |              |
 geonunit   | character varying(200) |                                                         | extended |              |
 gu_a3      | character varying(3)   |                                                         | extended |              |
 gn_id      | integer                |                                                         | plain    |              |
 gn_name    | character varying(200) |                                                         | extended |              |
 gns_id     | integer                |                                                         | plain    |              |
 gns_name   | character varying(200) |                                                         | extended |              |
 gn_level   | smallint               |                                                         | plain    |              |
 gn_region  | character varying(50)  |                                                         | extended |              |
 gn_a1_code | character varying(10)  |                                                         | extended |              |
 region_sub | character varying(250) |                                                         | extended |              |
 sub_code   | character varying(10)  |                                                         | extended |              |
 gns_level  | smallint               |                                                         | plain    |              |
 gns_lang   | character varying(10)  |                                                         | extended |              |
 gns_adm1   | character varying(10)  |                                                         | extended |              |
 gns_region | character varying(10)  |                                                         | extended |              |
 geom       | geometry(MultiPolygon) |                                                         | main     |              |
 id         | character varying      |                                                         | extended |              |
 l0_iso     | character varying      |                                                         | extended |              |
 l1_iso     | character varying      |                                                         | extended |              |



SELECT 'SELECT ' || array_to_string(ARRAY(SELECT 'o' || '.' || c.column_name
        FROM information_schema.columns As c
            WHERE table_name = 'ne_states'
            AND  c.column_name NOT IN('geom')
    ), ',') || ' FROM ne_states As o' As sqlstmt

update ne_states set gn_a1_code = NULL where gn_a1_code = iso_a2 || '.';
update ne_states set code_hasc = NULL where code_hasc = iso_a2 || '.';
update ne_states set iso_3166_2 = NULL where iso_3166_2 = iso_a2 || '-';

select
iso_a2 as l0_iso,
COALESCE(region_cod, gns_region) as l1_iso,
COALESCE(iso_3166_2, code_hasc, gn_a1_code) as l2_iso
from ne_states
order by l0_iso, l1_iso, l2_iso;
