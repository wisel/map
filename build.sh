#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CONTAINER=postgis
POSTGRES_USER=user
POSTGRES_PASSWORD=
POSTGRES_DB=postgis

source lib/functions.sh
#
# docker rm -f $CONTAINER
# docker run --name $CONTAINER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_DB=$POSTGRES_DB -d mdillon/postgis
# echo "Waiting for postgis to start"
# sleep 60s

title "Downloading files..."
mkdir -p $DIR/downloads
wget -nc -O $DIR/downloads/osm_fr_communes.zip http://osm13.openstreetmap.fr/~cquest/openfla/export/communes-20160119-shp.zip
#wget -nc -O $DIR/downloads/osm_fr_communes.zip http://osm13.openstreetmap.fr/~cquest/openfla/export/departements-20160218-shp.zip
wget -nc -O $DIR/downloads/osm_fr_departements.tar.gz http://export.openstreetmap.fr/contours-administratifs/france-departements.tar.gz
#wget -nc -O $DIR/downloads/osm_fr_departements.zip http://osm13.openstreetmap.fr/~cquest/openfla/export/regions-2016-shp.zip
wget -nc -O $DIR/downloads/osm_fr_regions.tar.gz http://export.openstreetmap.fr/contours-administratifs/france-regions.tar.gz
wget -nc -O $DIR/downloads/ne_countries.zip http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_admin_0_countries.zip
wget -nc -O $DIR/downloads/ne_states.zip http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_admin_1_states_provinces.zip

POSTGRES_HOST=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' $CONTAINER)
PGSQL="psql -q -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB"

title "Cleaning dist directory"
rm -Rfv "dist"

extract_shp "osm_fr_communes.zip" "osm_fr_communes" "ISO-8859-1"
extract_shp "osm_fr_departements.tar.gz" "osm_fr_departements" "ISO-8859-1"
extract_shp "osm_fr_regions.tar.gz" "osm_fr_regions" "ISO-8859-1"
extract_shp "ne_states.zip" "ne_states" "UTF-8"
extract_shp "ne_countries.zip" "ne_countries" "UTF-8"

$PGSQL < data/clean.sql
$PGSQL < data/fra.sql

# TODO: Use spatialite_patch instead, but we need to overhaul a lot of complex postgre SQL to Sqlite
postgis_patch "ne_countries"
postgis_patch "ne_states"
postgis_patch "osm_fr_communes"
postgis_patch "osm_fr_departements"
postgis_patch "osm_fr_regions"

spatialite_db "ne_states" "10_ne_states"
spatialite_db "osm_fr_communes" "00_osm_fr_communes"

ogr2ogr_filter "ne_countries" "world"
ogr2ogr_clip "ne_countries" "europe" -31.266001 27.636311 39.869301 81.008797
ogr2ogr_clip "ne_countries" "european_union" -12.33664 32.970699 38.0415 73.042122
ogr2ogr_clip "ne_countries" "africa" -25.35874 -46.900452 63.525379 37.56712
ogr2ogr_clip "ne_countries" "asia" 19.6381 -12.56111 180 82.50045
ogr2ogr_clip "ne_countries" "north_america" -167.276413 5.49955 -52.23304 83.162102
ogr2ogr_clip "ne_countries" "south_america" -109.47493 -59.450451 -26.33247 13.39029

for l0_iso in $(postgis_distinct "ne_states")
do
  ogr2ogr_filter "ne_states" "$l0_iso/all" "l0_iso='${l0_iso}'"
done

ogr2ogr_filter "osm_fr_regions" "FR/all_dom"
ogr2ogr_clip "osm_fr_regions" "FR/all" -5.1406 41.33374 9.55932 51.089062

for l1_iso in $(postgis_distinct "osm_fr_regions")
do
  ogr2ogr_filter "osm_fr_departements" "FR/$l1_iso/all" "l1_iso='${l1_iso}'"
done

for dir in $(postgis_distinct "osm_fr_departements")
do
  l2_iso=$(echo $dir | cut -d"/" -f2)
  ogr2ogr_filter "osm_fr_communes" "FR/$dir/all" "l2_iso='$l2_iso'"
done

for shp in $(find dist/ -name "*.shp")
do
  topojson_variants $shp
done
