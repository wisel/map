function title {
  echo -e "\e[31m$1\e[0m"
}

function extract_shp {
  title "Extracting $DIR/sources/$2.shp ..."
  mkdir -p $DIR/sources
  rm -Rf $DIR/tmp
  mkdir -p $DIR/tmp
  if [[ "$1" == *zip ]]
  then
    echo "zip"
    unzip $DIR/downloads/$1 -d $DIR/tmp
  else
    echo "tar"
    tar -xzvf $DIR/downloads/$1 -C $DIR/tmp
  fi
  rm -Rf $DIR/sources/$2*
  SHAPE_ENCODING=$3 ogr2ogr -f "ESRI Shapefile" $DIR/sources/$2.shp $DIR/tmp/*.shp
}

function ogr2ogr_filter {
  INPUT="sources/$1.shp"
  OUTPUT="dist/shapes/$2.shp"

  title "Filtering ($3) source $INPUT to $OUTPUT"
  if [ -e $OUTPUT ];
  then
    rm $OUTPUT
  fi
  mkdir -p $(dirname $OUTPUT)
  if [ -z "$3" ]
  then
    cp -f "sources/$1.shp" "dist/shapes/$2.shp"
    cp -f "sources/$1.shx" "dist/shapes/$2.shx"
    cp -f "sources/$1.prj" "dist/shapes/$2.prj"
    cp -f "sources/$1.dbf" "dist/shapes/$2.dbf"
  else
    ogr2ogr -f "ESRI Shapefile" -where "$3" $OUTPUT $INPUT
  fi
}

function ogr2ogr_clip {
  INPUT="sources/$1.shp"
  OUTPUT="dist/shapes/$2.shp"

  title "Clipping source $INPUT to $OUTPUT"
  if [ -e $OUTPUT ];
  then
    rm $OUTPUT
  fi
  mkdir -p $(dirname $OUTPUT)
  ogr2ogr -f "ESRI Shapefile" $OUTPUT $INPUT -clipsrc "$3" "$4" "$5" "$6"
}

function postgis_patch {
  INPUT="sources/$1.shp"
  OUTPUT="sources/$1.shp"
  TABLE="$1"
  PATCH="data/$1.sql"
  title "Patching source $INPUT to $OUTPUT"

  mkdir -p $(dirname $OUTPUT)
  shp2pgsql $INPUT $TABLE > tmp/out.sql
  echo "drop table IF EXISTS $TABLE;" | $PGSQL
  $PGSQL < tmp/out.sql 2>/dev/null
  $PGSQL < $PATCH
  pgsql2shp -f $OUTPUT -h$POSTGRES_HOST -u$POSTGRES_USER $POSTGRES_DB $TABLE
}

function spatialite_patch {
  INPUT="sources/$1"
  OUTPUT="sources/$1"
  TABLE="$1"
  PATCH="data/$1.sql"
  title "Patching source $INPUT.shp to $OUTPUT.shp"
  GEOMETRY_FIELD="geometry"
  DB="tmp/db.sqlite"

  mkdir -p $(dirname $OUTPUT)
  spatialite -silent $DB "DROP TABLE IF EXISTS $TABLE;"
  echo "spatialite_tool -i -shp $INPUT -d $DB -t $TABLE -c UTF-8 -g $GEOMETRY_FIELD"
  spatialite_tool -i -shp $INPUT -d $DB -t $TABLE -c UTF-8 -g $GEOMETRY_FIELD
  echo "patching..."
  spatialite -silent $DB < $PATCH
  echo "spatialite_tool -e -shp $OUTPUT -d $DB -t $TABLE -c UTF-8 -g $GEOMETRY_FIELD"
  spatialite_tool -e -shp $OUTPUT -d $DB -t $TABLE -c UTF-8 -g $GEOMETRY_FIELD --type POLYGON
}

function spatialite_distinct {
  TABLE="$1"
  DB="tmp/db.sqlite"
  PATCH="data/$1.distinct.sql"
  spatialite -silent $DB < $PATCH | sort | sed '/^\s*$/d'
}

function postgis_distinct {
  TABLE="$1"
  PATCH="data/$1.distinct.sql"
  $PGSQL -t < $PATCH | sort | sed '/^\s*$/d'
}

function topojson_variants {
  title "topojson from $filename"
  filename="${1%.*}"
  filename="${filename/dist\/shapes/dist\/topojson}"
  mkdir -p $(dirname $filename)
  mapshaper -i $1 encoding=UTF-8 id-field=id -simplify interval=10 -o $filename.high.json   format=topojson force
  mapshaper -i $1 encoding=UTF-8 id-field=id -simplify interval=250 -o $filename.medium.json format=topojson force
  mapshaper -i $1 encoding=UTF-8 id-field=id -simplify interval=1000 -o $filename.low.json    format=topojson force
}

function spatialite_db {
  INPUT="sources/$1"
  TABLE="$2"
  OUTPUT="dist/spatialite/db.sqlite"
  GEOMETRY_FIELD="geometry"
  mkdir -p $(dirname $OUTPUT)

  title "Spatialite $INPUT to $TABLE inside $OUTPUT"
  spatialite_tool -i -shp $INPUT -d $OUTPUT -t $TABLE -c UTF-8 -g $GEOMETRY_FIELD
  spatialite $OUTPUT "SELECT CreateSpatialIndex('$TABLE', '$GEOMETRY_FIELD');"
}
