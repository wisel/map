#!/bin/bash
source lib/functions.sh

api_key=$(<.api_key)

for file in $(find dist/binaries -name '*.zip')
do
  title "Uploading $file"
  http -a $api_key -f POST https://api.bitbucket.org/2.0/repositories/lighthouse-analytics/map/downloads/ files@$file
done
